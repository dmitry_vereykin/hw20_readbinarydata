/**
 * Created by Dmitry on 7/12/2015.
 */

import java.io.*;

public class ReadBinaryData {

    public static void main(String[] args) throws IOException {

        RandomAccessFile raf = new RandomAccessFile("Binary.dat", "r");

        System.out.println("Reading data from the file:\n");
        boolean bool = raf.readBoolean();
        byte by = raf.readByte();
        char ch = raf.readChar();
        double db = raf.readDouble();
        float fl = raf.readFloat();
        int integer = raf.readInt();
        long lg = raf.readLong();
        short sh = raf.readShort();
        String str = raf.readUTF();

        System.out.println("The boolean was " + bool);
        System.out.printf("The byte was hexadecimal %X\n", by);
        System.out.println("The char was " + ch);
        System.out.println("The double was " + db);
        System.out.println("The float was "+ fl);
        System.out.println("The int was " + integer);
        System.out.println("The long was " + lg);
        System.out.println("The short was " + sh);
        System.out.printf("The string was \"%s\"\n", str);

        raf.close();
        System.out.println("\nDone.");
    }
}
